/*
 * mesonskel-effect.h: A effect
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef MESONSKEL_EFFECT_H
#define MESONSKEL_EFFECT_H

mesonskel_effect_t *
mesonskel_effect_create();

void
mesonskel_effect_init(mesonskel_effect_t *effect);

void
mesonskel_effect_destroy(mesonskel_effect_t *effect);

#endif /* MESONSKEL_EFFECT_H */
