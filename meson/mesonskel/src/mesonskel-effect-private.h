/*
 * mesonskel-effect-private.h: A effect
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef MESONSKEL_EFFECT_H
#define MESONSKEL_EFFECT_H

/**
 * mesonskel_effect_t:
 *
 * Since: 0.0
 */
typedef struct _mesonskel_effect {
  // TODO
} mesonskel_effect_t;

#endif /* MESONSKEL_EFFECT_H */
