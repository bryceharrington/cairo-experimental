/*
 * mesonskel-surface.c: A surface
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:mesonskel-surface
 * @title: 
 * @short_description:  type
 *
 * #mesonskel_surface_t is a type representing...
 */

#include "mesonskel-surface-private.h"

/**
 * mesonskel_surface_create:
 *
 * Allocates a new #mesonskel_surface_t.
 *
 * The contents of the returned surface are undefined.
 *
 * @returns The newly allocated surface, or @c NULL on error.
 *
 * Since: 0.0
 */
mesonskel_surface_t *
mesonskel_surface_create(void) {
  struct mesonskel_surface_t *surface;

  surface = calloc(1, sizeof (*surface));
  if (surface == NULL)
    return NULL;

  return surface;
}

/**
 * mesonskel_surface_init:
 * @surface: A #mesonskel_surface_t to be initialized.
 *
 * Since: 0.0
 */
void
mesonskel_surface_init(mesonskel_surface_t *surface) {
  /* TODO: Initialize the structure's values */
}

/**
 * mesonskel_surface_destroy:
 * @surface: A #mesonskel_surface_t to be freed.
 *
 * Frees the resources allocated by mesonskel_surface_create().
 *
 * Since: 0.0
 */
void
mesonskel_surface_destroy(mesonskel_surface_t *surface) {
  free(surface);
}

