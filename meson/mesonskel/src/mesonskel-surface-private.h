/*
 * mesonskel-surface-private.h: A surface
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef MESONSKEL_SURFACE_H
#define MESONSKEL_SURFACE_H

/**
 * mesonskel_surface_t:
 *
 * Since: 0.0
 */
typedef struct _mesonskel_surface {
  // TODO
} mesonskel_surface_t;

#endif /* MESONSKEL_SURFACE_H */
