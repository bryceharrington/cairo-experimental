/*
 * mesonskel-compositor-private.h: A compositor
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef MESONSKEL_COMPOSITOR_H
#define MESONSKEL_COMPOSITOR_H

/**
 * mesonskel_compositor_t:
 *
 * Since: 0.0
 */
typedef struct _mesonskel_compositor {
  // TODO
} mesonskel_compositor_t;

#endif /* MESONSKEL_COMPOSITOR_H */
