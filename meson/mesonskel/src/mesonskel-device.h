/*
 * mesonskel-device.h: A device
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef MESONSKEL_DEVICE_H
#define MESONSKEL_DEVICE_H

mesonskel_device_t *
mesonskel_device_create();

void
mesonskel_device_init(mesonskel_device_t *device);

void
mesonskel_device_destroy(mesonskel_device_t *device);

#endif /* MESONSKEL_DEVICE_H */
