/*
 * mesonskel-path-private.h: A path
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef MESONSKEL_PATH_H
#define MESONSKEL_PATH_H

/**
 * mesonskel_path_t:
 *
 * Since: 0.0
 */
typedef struct _mesonskel_path {
  // TODO
} mesonskel_path_t;

#endif /* MESONSKEL_PATH_H */
