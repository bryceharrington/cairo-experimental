/*
 * mesonskel-compositor.h: A compositor
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
#ifndef MESONSKEL_COMPOSITOR_H
#define MESONSKEL_COMPOSITOR_H

mesonskel_compositor_t *
mesonskel_compositor_create();

void
mesonskel_compositor_init(mesonskel_compositor_t *compositor);

void
mesonskel_compositor_destroy(mesonskel_compositor_t *compositor);

#endif /* MESONSKEL_COMPOSITOR_H */
