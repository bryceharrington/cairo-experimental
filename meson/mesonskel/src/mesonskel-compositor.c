/*
 * mesonskel-compositor.c: A compositor
 *
 * Copyright © 2018 Bryce Harrington
 *
 * Released under mit, read the file 'COPYING' for more information.
 *
 * Contributors:
 */

/*
 * This file defines ...
 * It is based on ...
 * It is used by ...
 */
/**
 * SECTION:mesonskel-compositor
 * @title: 
 * @short_description:  type
 *
 * #mesonskel_compositor_t is a type representing...
 */

#include "mesonskel-compositor-private.h"

/**
 * mesonskel_compositor_create:
 *
 * Allocates a new #mesonskel_compositor_t.
 *
 * The contents of the returned compositor are undefined.
 *
 * @returns The newly allocated compositor, or @c NULL on error.
 *
 * Since: 0.0
 */
mesonskel_compositor_t *
mesonskel_compositor_create(void) {
  struct mesonskel_compositor_t *compositor;

  compositor = calloc(1, sizeof (*compositor));
  if (compositor == NULL)
    return NULL;

  return compositor;
}

/**
 * mesonskel_compositor_init:
 * @compositor: A #mesonskel_compositor_t to be initialized.
 *
 * Since: 0.0
 */
void
mesonskel_compositor_init(mesonskel_compositor_t *compositor) {
  /* TODO: Initialize the structure's values */
}

/**
 * mesonskel_compositor_destroy:
 * @compositor: A #mesonskel_compositor_t to be freed.
 *
 * Frees the resources allocated by mesonskel_compositor_create().
 *
 * Since: 0.0
 */
void
mesonskel_compositor_destroy(mesonskel_compositor_t *compositor) {
  free(compositor);
}

