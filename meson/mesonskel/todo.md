== Todo list for MesonSkel ==

* Fix build issues

* More/better detection logic for Cairo dependencies, both optional and
  required.
  + What to do about dependencies that are obsolescent/obsolete?

* Build configuration file

* Get the version handling to work more like Cairo does
  + (Or maybe explore other approaches than the even/odd scheme?)

* Testing
  + Experiment with making test cases using cmocka or other test
    frameworks, that might work better than Cairo's test framework
  + Several different types of testing needed:
    - Integration checks: Syntax, code style, API breaks, etc.  Cairo
      has lots of logic built in for this.
    - Unit tests: low level per-module code-only checks
    - Functional tests: Higher level, including rendering output
    - Low level performance testing: Discrete algorithms
    - High level performance testing: Against workloads
  + Need to think about how to support multiplatform.  Would be nice to
    run tests on a given platform and have decent coverage of
    platform-specific code for a lot of other platforms.
  + `make check` - Just runs integration checks (and maybe unit tests?)
  + `make distcheck` - Runs make check and produces a test dist package
  + `make test [$foo]` - run more thorough testing, which could be
    anything from a subset of the above to all available tests, and
    needs to be configurable to narrow focus to specific submodules,
    backends, platforms, etc.  See Cairo's test/README for ideas, but
    would be nice to have it be a bit easier to configure and run.

* docs: To customize the appearance, this generates the default
  stylesheet, header, and footer files for editing:
  + HTML:
    doxygen -w html header.html footer.html stylesheet.css <config_file>
  + Latex:
    doxygen -w latex header.tex footer.tex doxygen.sty <config_file>

* Release:  Needs to generate source package
  + See Cairo's RELEASING
  + Also refer to X.org's release.sh script
