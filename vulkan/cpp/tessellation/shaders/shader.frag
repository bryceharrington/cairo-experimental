#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 1) uniform sampler2D texSampler;

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

/* Use the texture for color mapping
void main() {
    outColor = texture(texSampler, fragTexCoord);
}
*/

/* Use the vertex's color fragment for color mapping */
void main() {
    outColor = vec4(fragColor, 1.0);
}
