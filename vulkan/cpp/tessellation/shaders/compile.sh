#!/bin/bash

VULKAN_PATH="${HOME}/foss/VulkanSDK/1.0.61.1/x86_64/bin"

${VULKAN_PATH}/glslangValidator -V shader.vert
${VULKAN_PATH}/glslangValidator -V shader.frag
