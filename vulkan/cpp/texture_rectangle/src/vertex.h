#ifndef VERTEX_H
#define VERTEX_H

#include "cglm.h"


struct Vertex {
    CGLMvec3 pos;
    CGLMvec3 color;
    CGLMvec3 texCoord;
};

#endif /* VERTEX_H */
