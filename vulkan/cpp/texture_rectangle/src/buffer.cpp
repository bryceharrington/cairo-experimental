#include "device.h"
#include "buffer.h"

bool buffer_create(Buffer *buffer, Device *device, VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties) {
    // Create the buffer holder
    VkBufferCreateInfo bufferInfo = {};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    buffer->vk_device = device->device;
    if (vkCreateBuffer(device->device, &bufferInfo, nullptr, &(buffer->vk_buffer)) != VK_SUCCESS) {
	fprintf(stderr, "Failed to create buffer!");
	return false;
    }

    // Query the buffer's memory requirements
    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(buffer->vk_device, buffer->vk_buffer, &memRequirements);

    // Allocate memory for the buffer
    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = device_find_memory_type(device, memRequirements.memoryTypeBits, properties);

    if (vkAllocateMemory(buffer->vk_device, &allocInfo, nullptr, &(buffer->vk_memory)) != VK_SUCCESS) {
	fprintf(stderr, "Failed to allocate buffer memory!");
	return false;
    }

    // Associate the memory with the buffer
    vkBindBufferMemory(device->device, buffer->vk_buffer, buffer->vk_memory, 0);
    return true;
}

bool buffer_destroy(Buffer *buffer) {
    vkDestroyBuffer(buffer->vk_device, buffer->vk_buffer, NULL);
    vkFreeMemory(buffer->vk_device, buffer->vk_memory, NULL);
}
