#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

// FIXME: Is there a better float define in GLFW?
#define GLfloat float

#include "cglm.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "utils.h"
#include "device.h"
#include "buffer.h"
#include "vertex.h"
#include "image.h"

#include <assert.h>
#include <time.h>

const int WIDTH = 800;
const int HEIGHT = 600;

static VkVertexInputBindingDescription vertex_get_binding_description() {
    VkVertexInputBindingDescription bindingDescription = {};
    bindingDescription.binding = 0;
    bindingDescription.stride = sizeof(Vertex);
    bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX; // Move to next data after each vertex

    return bindingDescription;
}

bool
get_required_extensions(const char *extensions[], uint32_t *num_extensions) {
    // Take all of GLFW's required extensions as required
    const char **glfwExtensions;

    glfwExtensions = glfwGetRequiredInstanceExtensions(num_extensions);
    assert(*num_extensions < MAX_ARRAY);

    for (uint32_t i=0; i < *num_extensions; i++) {
	extensions[i] = glfwExtensions[i];
    }

    // TODO: Can I do ARRAY_LENGTH(extensions)?
    if (enableValidationLayers) {
	// Only take debug extension if validation layers are enabled
	extensions[*num_extensions] = VK_EXT_DEBUG_REPORT_EXTENSION_NAME;
	(*num_extensions)++;
    }

    return true;
}

struct UniformBufferObject {
    CGLMmat4 model;
    CGLMmat4 view;
    CGLMmat4 proj;
};

const Vertex vertices[] = {
    {{-0.5f, -0.5f, 0.0f}, {1.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f}},
    {{ 0.5f, -0.5f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}},
    {{ 0.5f,  0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f, 0.0f}},
    {{-0.5f,  0.5f, 0.0f}, {1.0f, 1.0f, 1.0f}, {1.0f, 1.0f, 0.0f}}
};

const uint16_t num_indices = 6;
const uint16_t indices[num_indices] = {
    0, 1, 2, 2, 3, 0
};

/* Surface format defines the color depth to use
 */
VkSurfaceFormatKHR choose_swap_surface_format(VkSurfaceFormatKHR *available_formats, uint32_t num_formats) {
    // Surface has no preferred format, so use our own preferences
    if (num_formats == 1 && available_formats[0].format == VK_FORMAT_UNDEFINED) {
	return {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
    }

    // See if our preferred combination is in the surface's available format list
    for (uint32_t i=0; i<num_formats; i++) {
	if (available_formats[i].format == VK_FORMAT_B8G8R8A8_UNORM
	    && available_formats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
	    return available_formats[i];
	}
    }

    // Give up, just settle for the first format specified
    return available_formats[0];
}

/* Presentation mode represents the actual conditions for showing images to the screen.
 */
VkPresentModeKHR choose_swap_present_mode(VkPresentModeKHR *available_present_modes, uint32_t num_modes) {
    VkPresentModeKHR best_mode = VK_PRESENT_MODE_FIFO_KHR; // Guaranteed to be available

    for (uint32_t i=0; i<num_modes; i++) {
	// Prefer triple buffering if it is available
	if (available_present_modes[i] == VK_PRESENT_MODE_MAILBOX_KHR) {
	    return available_present_modes[i];
	} else if (available_present_modes[i] == VK_PRESENT_MODE_IMMEDIATE_KHR) {
	    best_mode = available_present_modes[i];
	}
    }

    return best_mode;
}

/* Swap extent is the resolution of the swap chain images.
 * Typically equals the resolution of the window we're drawing to.
 */
VkExtent2D choose_swap_extent(const VkSurfaceCapabilitiesKHR &capabilities, int32_t width, int32_t height) {
    if (capabilities.currentExtent.width != UINT64_MAX) {
	return capabilities.currentExtent;
    } else {
	VkExtent2D actualExtent = {(uint32_t)width, (uint32_t)height};

	// Clamp the width & height values to allowed maximums for the implementation
	actualExtent.width = CLAMP(actualExtent.width,
				   capabilities.minImageExtent.width,
				   capabilities.maxImageExtent.width);
	actualExtent.height = CLAMP(actualExtent.height,
				    capabilities.minImageExtent.height,
				    capabilities.maxImageExtent.height);
	return actualExtent;
    }
}

struct Context {
    Device _device;

//private:
    VkSwapchainKHR swapChain;
    uint32_t num_swap_chain_images;
    VkImage swap_chain_images[MAX_ARRAY];
    VkFormat swapChainImageFormat;
    VkExtent2D swapChainExtent;

    uint32_t num_swap_chain_image_views;
    VkImageView swap_chain_image_views[MAX_ARRAY];

    uint32_t num_swap_chain_framebuffers;
    VkFramebuffer swap_chain_framebuffers[MAX_ARRAY];

    VkRenderPass renderPass;
    VkDescriptorSetLayout descriptorSetLayout;
    VkPipelineLayout pipelineLayout;
    VkPipeline graphicsPipeline;

    VkCommandPool commandPool;

    Image texture_image;
    VkImageView textureImageView;
    VkSampler textureSampler;

    Buffer   vertex_buffer;
    Buffer   index_buffer;
    Buffer   uniform_buffer;

    VkDescriptorPool descriptorPool;
    VkDescriptorSet descriptorSet;

    uint32_t num_command_buffers;
    VkCommandBuffer command_buffers[MAX_ARRAY];

    VkSemaphore imageAvailableSemaphore;
    VkSemaphore renderFinishedSemaphore;
};

static VKAPI_ATTR VkBool32 VKAPI_CALL debug_callback(VkDebugReportFlagsEXT flags,
						     VkDebugReportObjectTypeEXT objType,
						     uint64_t obj,
						     size_t location,
						     int32_t code,
						     const char *layerPrefix,
						     const char *msg,
						     void *userData) {
    fprintf(stderr, "validation layer: %s\n", msg);

    return VK_FALSE;
}

/** Load a given SPIR-V file as shader module
 */
bool _context_load_shader_module(Context *ctx, const char *filename, VkShaderModule *shaderModule) {
    FILE *fp;
    size_t file_size, num_read, buffer_size;

    fp = fopen(filename, "rb");
    if (fp == NULL) {
	fprintf(stderr, "Could not open SPIR-V file %s\n", filename);
	return false;
    }

    if (fseek(fp, 0, SEEK_END) != 0) {
	// TODO: check errno
	return false;
    }
    file_size = ftell(fp);
    if (file_size <= 0) {
	// TODO: Check errno
	return false;
    }
    if (fseek(fp, 0, SEEK_SET) != 0) {
	// TODO: Check errno
	return false;
    }

    // Determine size of the file, and allocate a buffer
    buffer_size = (long)(ceil(file_size / 4.0)) * 4;
    char *buffer = (char*)malloc(buffer_size * sizeof(char));
    if (buffer == NULL) {
	return false;
    }

    // Read in the data
    num_read = fread(buffer, sizeof(char), file_size, fp);
    fclose(fp);

    if (num_read != file_size) {
	if (feof(fp) != 0) {
	    fprintf(stderr, "Size of data read from %s did not match file size.\n", filename);
	} else if (ferror(fp)) {
	    fprintf(stderr, "Error indicator was set while trying to read %s\n", filename);
	} else {
	    fprintf(stderr, "Unknown error reading from file %s: %ld read, but file size is %ld\n",
		    filename, num_read, file_size);
	}
	free(buffer);
	return false;
    }

    // Pad data
    for (size_t i=file_size; i<buffer_size; i++)
	buffer[i] = 0;

    VkShaderModuleCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = file_size;
    createInfo.pCode = (const uint32_t*)buffer;

    if (vkCreateShaderModule(ctx->_device.device, &createInfo, nullptr, shaderModule) != VK_SUCCESS) {
	fprintf(stderr, "Failed to create shader module");
	return false;
    }

    return true;
}

void _context_cleanup_swap_chain(Context *ctx) {
    for (size_t i=0; i<ctx->num_swap_chain_framebuffers; i++) {
	vkDestroyFramebuffer(ctx->_device.device, ctx->swap_chain_framebuffers[i], nullptr);
    }

    vkFreeCommandBuffers(ctx->_device.device, ctx->commandPool, ctx->num_command_buffers, ctx->command_buffers);

    vkDestroyPipeline(ctx->_device.device, ctx->graphicsPipeline, nullptr);
    vkDestroyPipelineLayout(ctx->_device.device, ctx->pipelineLayout, nullptr);
    vkDestroyRenderPass(ctx->_device.device, ctx->renderPass, nullptr);
    for (size_t i=0; i<ctx->num_swap_chain_image_views; i++) {
	vkDestroyImageView(ctx->_device.device, ctx->swap_chain_image_views[i], nullptr);
    }

    vkDestroySwapchainKHR(ctx->_device.device, ctx->swapChain, nullptr);
}

bool context_init_device_instance(Context *ctx) {
    uint32_t num_extensions = 0;
    const char *extensions[MAX_ARRAY];

    get_required_extensions(extensions, &num_extensions);
    assert(num_extensions != 0);
    device_create_instance(&ctx->_device, extensions, num_extensions);
    if (! device_setup_debug_callback(&ctx->_device, debug_callback))
	return false;
    return true;
}

bool _context_create_swap_chain(Context *ctx, int32_t width, int32_t height) {
    SwapChainSupportDetails swapChainSupport = device_get_swap_chain_support(&ctx->_device);

    VkSurfaceFormatKHR surfaceFormat = choose_swap_surface_format(swapChainSupport.formats,
								  swapChainSupport.num_formats);
    VkPresentModeKHR presentMode = choose_swap_present_mode(swapChainSupport.present_modes,
							    swapChainSupport.num_present_modes);
    VkExtent2D extent = choose_swap_extent(swapChainSupport.capabilities, width, height);

    // Decide number of images in the swap chain (i.e. queue length)
    uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
    if (swapChainSupport.capabilities.maxImageCount > 0
	&& imageCount > swapChainSupport.capabilities.maxImageCount) {
	imageCount = swapChainSupport.capabilities.maxImageCount;
    }

    // Create the swap chain object
    VkSwapchainCreateInfoKHR createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = ctx->_device.surface;
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = surfaceFormat.format;
    createInfo.imageColorSpace = surfaceFormat.colorSpace;
    createInfo.imageExtent = extent;
    createInfo.imageArrayLayers = 1;

    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    /* FIXME: Use VK_IMAGE_USAGE_TRANSFER_DST_BIT instead, to allow operations
     * like post-processing, and then use a memory operation to transfer the
     * rendered image to a swap chain image.
     */

    QueueFamilyIndices all_indices = device_get_queue_families(&ctx->_device);
    uint32_t queue_family_indices[] = {(uint32_t) all_indices.graphicsFamily,
				       (uint32_t) all_indices.presentFamily};

    if (all_indices.graphicsFamily != all_indices.presentFamily) {
	/* If the graphics queue family is different from the
	 * presentation queue, then specify how to handle swap chain
	 * images used across multiple queue families.  Images can
	 * be used across multiple queue families without explicit
	 * ownership transfers.
	 */
	createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
	createInfo.queueFamilyIndexCount = 2;
	createInfo.pQueueFamilyIndices = queue_family_indices;
	// FIXME: Handle ownership, to permit better performance in this case
    } else {
	/* An image is owned by one queue family at a time, and
	 * ownership must be explicitly transferred before using it
	 * in another queue family.
	 */
	createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	createInfo.queueFamilyIndexCount = 0; // Optional
	createInfo.pQueueFamilyIndices = nullptr; // Optional
    }

    // Ignore image transformations
    createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
    // FIXME: Apply transforms to images if supported, e.g. rotations/flips

    // Ignore the alpha channel (don't belnd with other windows in the window system)
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

    createInfo.presentMode = presentMode;

    // Ignore color of obscured pixels, so just clip them for best performance
    createInfo.clipped = VK_TRUE;

    // Ignore recreation of the swap chain (for now)
    createInfo.oldSwapchain = VK_NULL_HANDLE;
    // FIXME: Handle recreation of the swap chain using the old one
    //        (e.g. when window is resized)

    if (vkCreateSwapchainKHR(ctx->_device.device, &createInfo, nullptr, &ctx->swapChain) != VK_SUCCESS) {
	fprintf(stderr, "Failed to create swap chain!\n");
	return false;
    }

    // Get the number of images in the chain (that we set with minImageCount earlier),
    // then resize the container to match and retrieve the handles.
    vkGetSwapchainImagesKHR(ctx->_device.device, ctx->swapChain, &imageCount, nullptr);
    ctx->num_swap_chain_images = imageCount;
    assert(ctx->num_swap_chain_images < MAX_ARRAY);
    vkGetSwapchainImagesKHR(ctx->_device.device, ctx->swapChain, &imageCount, ctx->swap_chain_images);

    ctx->swapChainImageFormat = surfaceFormat.format;
    ctx->swapChainExtent = extent;
    return true;
}

bool _context_create_render_pass(Context *ctx) {
    // Define the framebuffer attachments to use while rendering.
    // We'll have one color buffer attachment only, represented by one image from the swap chain
    VkAttachmentDescription colorAttachment = {};
    colorAttachment.format = ctx->swapChainImageFormat;
    colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;            // Clear data in attachment at start
    colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;          // Store rendered content for reading later
    colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE; // We're not using the stencil buffer
    colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;   // Present images in the swap chain

    VkAttachmentReference colorAttachmentRef = {};
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    // Render subpasses allow sequential post-processing effects applied one after another.
    // Grouping them permits Vulkan to reorder them to conserve memory and improve performance
    // We only need one single subpass for this demo
    VkSubpassDescription subpass = {};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;     // There are also Compute subpasses
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;
    // Other kinds of attachments that can be referenced include ones
    // for shaders, multisampling color, depth and stencil data, etc.

    // Define where our subpass dependency occurs relative to the render pass
    VkSubpassDependency dependency = {};
    dependency.srcSubpass = VK_SUBPASS_EXTERNAL; // Do subpass before the render pass
    dependency.dstSubpass = 0;

    // Operations to wait on and stages when op will occur.  We're
    // going to wait on the color attachment output stage itself.
    dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.srcAccessMask = 0;

    // Wait on the reading and writing of the color attachment
    dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    // Create the render pass itself
    VkRenderPassCreateInfo renderPassInfo = {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = 1;
    renderPassInfo.pAttachments = &colorAttachment;
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;

    // Add the subpass dependency to the renderpass
    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &dependency;

    if (vkCreateRenderPass(ctx->_device.device, &renderPassInfo, nullptr, &ctx->renderPass) != VK_SUCCESS) {
	fprintf(stderr, "Failed to create render pass!\n");
	return false;
    }
    return true;
}

bool _context_create_descriptor_set_layout(Context *ctx) {
    VkDescriptorSetLayoutBinding uboLayoutBinding = {};
    uboLayoutBinding.binding = 0;
    uboLayoutBinding.descriptorCount = 1;
    uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboLayoutBinding.pImmutableSamplers = nullptr;
    uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    VkDescriptorSetLayoutBinding samplerLayoutBinding = {};
    samplerLayoutBinding.binding = 1;
    samplerLayoutBinding.descriptorCount = 1;
    samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerLayoutBinding.pImmutableSamplers = nullptr;
    samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    const VkDescriptorSetLayoutBinding bindings[] = { uboLayoutBinding, samplerLayoutBinding };
    VkDescriptorSetLayoutCreateInfo layoutInfo = {};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = ARRAY_LENGTH(bindings);
    layoutInfo.pBindings = bindings;

    if (vkCreateDescriptorSetLayout(ctx->_device.device, &layoutInfo, nullptr, &ctx->descriptorSetLayout) != VK_SUCCESS) {
	fprintf(stderr, "Failed to create descriptor set layout!\n");
	return false;
    }

    return true;
}

bool context_create_graphics_pipeline(Context *ctx) {
    // Read in the compiled vertex and fragment shader files
    VkShaderModule vertShaderModule;
    VkShaderModule fragShaderModule;
    _context_load_shader_module(ctx, "shaders/vert.spv", &vertShaderModule);
    _context_load_shader_module(ctx, "shaders/frag.spv", &fragShaderModule);

    VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
    vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
    vertShaderStageInfo.module = vertShaderModule;
    vertShaderStageInfo.pName = "main";

    VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
    fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    fragShaderStageInfo.module = fragShaderModule;
    fragShaderStageInfo.pName = "main";

    VkPipelineShaderStageCreateInfo shaderStages[] = {
	vertShaderStageInfo,
	fragShaderStageInfo
    };

    VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
    vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

    VkVertexInputBindingDescription bindingDescription = vertex_get_binding_description();

    VkVertexInputAttributeDescription attributeDescriptions[3] = {};

    // Position
    attributeDescriptions[0].binding = 0;
    attributeDescriptions[0].location = 0;
    attributeDescriptions[0].format = VK_FORMAT_R32G32_SFLOAT;
    attributeDescriptions[0].offset = offsetof(Vertex, pos);

    // Color
    attributeDescriptions[1].binding = 0;
    attributeDescriptions[1].location = 1;
    attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
    attributeDescriptions[1].offset = offsetof(Vertex, color);

    // Texture
    attributeDescriptions[2].binding = 0;
    attributeDescriptions[2].location = 2;
    attributeDescriptions[2].format = VK_FORMAT_R32G32_SFLOAT;
    attributeDescriptions[2].offset = offsetof(Vertex, texCoord);

    // Define format of the vertex data we'll be passing to the vertex shader
    vertexInputInfo.vertexBindingDescriptionCount = 1;
    vertexInputInfo.vertexAttributeDescriptionCount = ARRAY_LENGTH(attributeDescriptions);
    vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
    vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions;

    // Define geometry to draw from vertices, and whether to enable primitive restart
    // We'll just be drawing triangles for this demo
    VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
    inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    inputAssembly.primitiveRestartEnable = VK_FALSE;

    // Define viewports, which defines the part of the framebuffer output is rendered to.
    // Output will be transformed/scaled to fit the region.
    // We'll be drawing to the entire framebuffer, from 0,0 to W,H
    VkViewport viewport = {};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = (float) ctx->swapChainExtent.width;
    viewport.height = (float) ctx->swapChainExtent.height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    // Define the scissor rectangle for the viewport.
    // Output will be clipped to this rectangle.
    // We want to draw the full framebuffer, so just make scissor cover it entirely
    VkRect2D scissor = {};
    scissor.offset = {0, 0};
    scissor.extent = ctx->swapChainExtent;

    // Combine the viewport and scissor rectangle
    VkPipelineViewportStateCreateInfo viewportState = {};
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    // Set up the rasterizer.  This processes vertex shader output
    // for the fragment shader.
    VkPipelineRasterizationStateCreateInfo rasterizer = {};
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = VK_FALSE;         // Useful for special cases like shadow maps
    rasterizer.rasterizerDiscardEnable = VK_FALSE;  // Disables rasterization
    rasterizer.polygonMode = VK_POLYGON_MODE_FILL;  // Options are fill, line, or point
    rasterizer.lineWidth = 1.0f;                    // Thickness of lines in fragments
    rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;    // Options are disable, front, back, or both
    rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE; // Vertex order for selecting front faces
    rasterizer.depthBiasEnable = VK_FALSE;          // Depth bias used for shadow mapping
    rasterizer.depthBiasConstantFactor = 0.0f;
    rasterizer.depthBiasClamp = 0.0f;
    rasterizer.depthBiasSlopeFactor = 0.0f;

    // Multisampling (anti-aliasing)
    // We won't be using multisampling for the demo
    VkPipelineMultisampleStateCreateInfo multisampling = {};
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampling.minSampleShading = 1.0f;
    multisampling.pSampleMask = nullptr;
    multisampling.alphaToCoverageEnable = VK_FALSE;
    multisampling.alphaToOneEnable = VK_FALSE;

    // FIXME: Depth and stencil testing.  Configure the VkPipelineDepthStencilStateCreateInfo structure.
    // For this demo we'll just pass NULL for that, and come back to it later.

    // Color blending.
    // First structure configures blending per attached framebuffer
    // Second one contains the global color blending settings
    VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
    colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT
	| VK_COLOR_COMPONENT_G_BIT
	| VK_COLOR_COMPONENT_B_BIT
	| VK_COLOR_COMPONENT_A_BIT;

    VkPipelineColorBlendStateCreateInfo colorBlending = {};
    colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.logicOp = VK_LOGIC_OP_COPY;
    colorBlending.attachmentCount = 1;
    colorBlending.pAttachments = &colorBlendAttachment;
    colorBlending.blendConstants[0] = 0.0f;
    colorBlending.blendConstants[1] = 0.0f;
    colorBlending.blendConstants[2] = 0.0f;
    colorBlending.blendConstants[3] = 0.0f;

    // Pipeline layout
    // This demo doesn't use them, so just create an empty pipeline layout
    VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = 1;
    pipelineLayoutInfo.pSetLayouts = &ctx->descriptorSetLayout;
    pipelineLayoutInfo.pushConstantRangeCount = 0;
    pipelineLayoutInfo.pPushConstantRanges = 0;

    if (vkCreatePipelineLayout(ctx->_device.device, &pipelineLayoutInfo, nullptr, &ctx->pipelineLayout) != VK_SUCCESS) {
	fprintf(stderr, "Failed to create pipeline layout!\n");
	return false;
    }

    // Combine all the various objects to create the graphics pipeline
    VkGraphicsPipelineCreateInfo pipelineInfo = {};
    pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount = 2;
    pipelineInfo.pStages = shaderStages;
    pipelineInfo.pVertexInputState = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pDepthStencilState = nullptr;
    pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.pDynamicState = nullptr;

    // Reference structs describing the fixed-function stage
    pipelineInfo.layout = ctx->pipelineLayout;

    // Pipeline layout
    pipelineInfo.renderPass = ctx->renderPass;
    pipelineInfo.subpass = 0;

    // Render pass
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;

    // Create the pipeline itself.
    // We won't use the pipeline cache support for this demo (just pass VK_NULL_HANDLE)
    // but could be more performant to cache pipelines we've established for higher performance.
    if (vkCreateGraphicsPipelines(ctx->_device.device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &ctx->graphicsPipeline) != VK_SUCCESS) {
	fprintf(stderr, "Failed to create graphics pipeline!\n");
	return false;
    }

    vkDestroyShaderModule(ctx->_device.device, fragShaderModule, nullptr);
    vkDestroyShaderModule(ctx->_device.device, vertShaderModule, nullptr);
    return true;
}

bool _context_create_framebuffers(Context *ctx) {
    assert(ctx->num_swap_chain_image_views < MAX_ARRAY);
    /* TODO
       swapChainFramebuffers.resize(swapChainImageViews.size());
    */
    ctx->num_swap_chain_framebuffers = ctx->num_swap_chain_image_views;

    for (size_t i=0; i<ctx->num_swap_chain_image_views; i++) {
	VkImageView attachments[] = {
	    ctx->swap_chain_image_views[i]
	};

	VkFramebufferCreateInfo framebufferInfo = {};
	framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	framebufferInfo.renderPass = ctx->renderPass;
	framebufferInfo.attachmentCount = 1;
	framebufferInfo.pAttachments = attachments;
	framebufferInfo.width = ctx->swapChainExtent.width;
	framebufferInfo.height = ctx->swapChainExtent.height;
	framebufferInfo.layers = 1;

	if (vkCreateFramebuffer(ctx->_device.device, &framebufferInfo, nullptr, &ctx->swap_chain_framebuffers[i]) != VK_SUCCESS) {
	    fprintf(stderr, "Failed to create framebuffer!\n");
	    return false;
	}
    }

    return true;
}

bool _context_create_command_pool(Context *ctx) {
    QueueFamilyIndices queueFamilyIndices = device_get_queue_families(&ctx->_device);

    // Pool info lets us customize how the pool can be manipulated.
    // The TRANSIENT flag would allow command buffers to be rerecorded frequently.
    // The RESET_COMMAND_BUFFER flag would allow buffers to be rerecorded individually.
    VkCommandPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;
    poolInfo.flags = 0;
    poolInfo.pNext = NULL;

    if (vkCreateCommandPool(ctx->_device.device, &poolInfo, nullptr, &ctx->commandPool) != VK_SUCCESS) {
	fprintf(stderr, "Failed to create command pool!\n");
	return false;
    }
    return true;
}

VkCommandBuffer _context_begin_single_time_commands(Context *ctx) {
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = ctx->commandPool;
    allocInfo.commandBufferCount = 1;

    // Setup a command buffer for the copy command
    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(ctx->_device.device, &allocInfo, &commandBuffer);

    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(commandBuffer, &beginInfo);

    return commandBuffer;
}

void _context_end_single_time_commands(Context *ctx, VkCommandBuffer commandBuffer) {
    vkEndCommandBuffer(commandBuffer);

    // Execute the command buffer to complete the transfer
    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    vkQueueSubmit(ctx->_device.graphics_queue, 1, &submitInfo, VK_NULL_HANDLE);

    // There's no events we need to wait on, just execute
    // immediately, and wait for the queue to become idle.
    vkQueueWaitIdle(ctx->_device.graphics_queue);
    // Another approach permitting better optimizations would be to
    // use a fence, to schedule multiple transfers simultaneously,
    // instead of executing them one by one.

    vkFreeCommandBuffers(ctx->_device.device, ctx->commandPool, 1, &commandBuffer);
}

/** Handles layout transitions */
bool _context_transition_image_layout(Context *ctx, VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout) {
    VkCommandBuffer commandBuffer = _context_begin_single_time_commands(ctx);

    VkImageMemoryBarrier barrier = {};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.oldLayout = oldLayout;
    barrier.newLayout = newLayout;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED; // We're not transferring queue family ownership
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED; // so ignore these two fields.
    barrier.image = image;
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;
    barrier.srcAccessMask = 0;
    barrier.dstAccessMask = 0;

    // Set the access masks and pipeline stages for the transition
    VkPipelineStageFlags sourceStage;
    VkPipelineStageFlags destinationStage;
    if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED &&
	newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
	// Handle Undefined -> Transfer destination
	// These don't need to wait on anything
	barrier.srcAccessMask = 0;
	barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

	sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
	destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL &&
	       newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
	// Handle Transfer destination -> Shader reading
	// The reads should wait on transfer writes
	barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

	sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    } else {
	fprintf(stderr, "Unsupported layout transition!\n");
	return false;
    }

    vkCmdPipelineBarrier(commandBuffer,
			 sourceStage, destinationStage,
			 0,
			 0, nullptr,
			 0, nullptr,
			 1, &barrier
			 );

    _context_end_single_time_commands(ctx, commandBuffer);

    /*
     * All of the helper functions that submit commands so far have
     * been set up to execute synchronously by waiting for the queue to
     * become idle. For practical applications it is recommended to combine
     * these operations in a single command buffer and execute them
     * asynchronously for higher throughput, especially the transitions and
     * copy in the createTextureImage function. Try to experiment with this
     * by creating a setupCommandBuffer that the helper functions record
     * commands into, and add a flushSetupCommands to execute the commands
     * that have been recorded so far. It's best to do this after the
     * texture mapping works to check if the texture resources are still
     * set up correctly.
     */
    return true;
}

void _context_copy_buffer(Context *ctx, VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size) {
    VkCommandBuffer commandBuffer = _context_begin_single_time_commands(ctx);

    // Create a Copy command to transfer the contents of our buffer
    VkBufferCopy copyRegion = {};
    copyRegion.srcOffset = 0;
    copyRegion.dstOffset = 0;
    copyRegion.size = size;
    vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

    _context_end_single_time_commands(ctx, commandBuffer);
}

void _context_copy_buffer_to_image(Context *ctx, VkBuffer buffer, VkImage image, uint32_t width, uint32_t height) {
    VkCommandBuffer commandBuffer = _context_begin_single_time_commands(ctx);

    // Specify which part of the buffer is going to be copied to which part of the image
    VkBufferImageCopy region = {};
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;

    region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;

    region.imageOffset = {0, 0, 0};
    region.imageExtent = {
	width,
	height,
	1
    };

    vkCmdCopyBufferToImage(commandBuffer,
			   buffer,
			   image,
			   VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			   1,
			   &region
			   );

    _context_end_single_time_commands(ctx, commandBuffer);
}

bool _context_create_texture_image(Context *ctx) {
    int texWidth, texHeight, texChannels;
    stbi_uc *pixels = stbi_load("textures/texture.jpg",
				&texWidth, &texHeight, &texChannels,
				STBI_rgb_alpha);
    VkDeviceSize imageSize = texWidth * texHeight * 4;
    Buffer staging_buffer;

    if (!pixels) {
	fprintf(stderr, "Failed to load texture image!\n");
	return false;
    }

    if (!buffer_create(&staging_buffer, &ctx->_device, imageSize,
		       VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		       VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)) {
	return false;
    }

    void *data;
    vkMapMemory(ctx->_device.device, staging_buffer.vk_memory, 0, imageSize, 0, &data);
    memcpy(data, pixels, static_cast<size_t>(imageSize));
    vkUnmapMemory(ctx->_device.device, staging_buffer.vk_memory);

    stbi_image_free(pixels);

    image_create(&ctx->texture_image, &ctx->_device, texWidth, texHeight,
		 VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL,
		 VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
		 VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    _context_transition_image_layout(ctx,
				     ctx->texture_image.vk_image,
				     VK_FORMAT_R8G8B8A8_UNORM,
				     VK_IMAGE_LAYOUT_UNDEFINED,               // Old layout
				     VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL     // New layout
				     );
    _context_copy_buffer_to_image(ctx,
				  staging_buffer.vk_buffer, ctx->texture_image.vk_image,
				  static_cast<uint32_t>(texWidth),
				  static_cast<uint32_t>(texHeight));
    _context_transition_image_layout(ctx,
				     ctx->texture_image.vk_image,
				     VK_FORMAT_R8G8B8A8_UNORM,
				     VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,    // Old layout
				     VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL // New layout
				     );

    // Cleanup the staging buffer and its memory
    buffer_destroy(&staging_buffer);
    return true;
}

bool _context_create_image_view(Context *ctx, VkImageView *imageView, VkImage image, VkFormat format) {
    VkImageViewCreateInfo viewInfo = {};
    viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    viewInfo.image = image;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format = format;
    viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = 1;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = 1;

    if (vkCreateImageView(ctx->_device.device, &viewInfo, nullptr, imageView) != VK_SUCCESS) {
	fprintf(stderr, "Failed to create image view!");
	return false;
    }

    return true;
}

bool _context_create_image_views(Context *ctx) {
    assert(ctx->num_swap_chain_images < MAX_ARRAY);
    /* TODO
       swapChainImageViews.resize(num_swap_chain_images);
    */
    ctx->num_swap_chain_image_views = ctx->num_swap_chain_images;

    for (size_t i=0; i<ctx->num_swap_chain_images; i++) {
	if (! _context_create_image_view(ctx,
					&ctx->swap_chain_image_views[i],
					ctx->swap_chain_images[i],
					ctx->swapChainImageFormat))
	    return false;
    }

    return true;
}

bool _context_create_texture_image_view(Context *ctx) {
    return _context_create_image_view(ctx,
				      &ctx->textureImageView,
				      ctx->texture_image.vk_image,
				      VK_FORMAT_R8G8B8A8_UNORM);
}

bool _context_create_texture_sampler(Context *ctx) {
    VkSamplerCreateInfo samplerInfo = {};
    samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerInfo.magFilter = VK_FILTER_LINEAR; // How to interpolate texels that are magnified
    samplerInfo.minFilter = VK_FILTER_LINEAR; // ... and minified.
    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT; // How texture should repeat
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.anisotropyEnable = VK_TRUE;
    samplerInfo.maxAnisotropy = 16;
    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerInfo.unnormalizedCoordinates = VK_FALSE; // Whether coordinates range from 0,1 or 0,DIM
    samplerInfo.compareEnable = VK_FALSE;
    samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR; // Mipmapping
    samplerInfo.mipLodBias = 0.0f;
    samplerInfo.minLod = 0.0f;
    samplerInfo.maxLod = 0.0f;

    if (vkCreateSampler(ctx->_device.device, &samplerInfo, nullptr, &ctx->textureSampler) != VK_SUCCESS) {
	fprintf(stderr, "Failed to create texture sampler!\n");
	return false;
    }
    return true;
}

bool _context_create_vertex_buffer(Context *ctx) {
    VkDeviceSize bufferSize = sizeof(vertices);

    // Use a staging buffer for mapping and copying the vertex data
    Buffer staging_buffer;
    if (!buffer_create(&staging_buffer, &ctx->_device, bufferSize,
		       VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		       VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)) {
	return false;
    }

    // Copy the vertices to the staging buffer
    void *data;
    vkMapMemory(ctx->_device.device, staging_buffer.vk_memory, 0, bufferSize, 0, &data);
    memcpy(data, vertices, (size_t) bufferSize);
    vkUnmapMemory(ctx->_device.device, staging_buffer.vk_memory);

    // Create the destination vertex buffer for the graphics device
    if (!buffer_create(&ctx->vertex_buffer, &ctx->_device, bufferSize,
		       VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
		       VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)) {
	return false;
    }

    // Move vertex staging data to the device local buffer
    _context_copy_buffer(ctx, staging_buffer.vk_buffer, ctx->vertex_buffer.vk_buffer, bufferSize);

    // Cleanup
    buffer_destroy(&staging_buffer);
    return true;
}

bool _context_create_index_buffer(Context *ctx) {
    VkDeviceSize bufferSize = sizeof(indices[0]) * num_indices;

    Buffer staging_buffer;
    if (!buffer_create(&staging_buffer, &ctx->_device, bufferSize,
		       VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		       VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)) {
	return false;
    }

    void *data;
    vkMapMemory(ctx->_device.device, staging_buffer.vk_memory, 0, bufferSize, 0, &data);
    memcpy(data, indices, (size_t) bufferSize);
    vkUnmapMemory(ctx->_device.device, staging_buffer.vk_memory);

    if (!buffer_create(&ctx->index_buffer, &ctx->_device, bufferSize,
		       VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
		       VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)) {
	return false;
    }

    _context_copy_buffer(ctx, staging_buffer.vk_buffer, ctx->index_buffer.vk_buffer, bufferSize);

    buffer_destroy(&staging_buffer);
    return true;
}

bool _context_create_uniform_buffer(Context *ctx) {
    VkDeviceSize bufferSize = sizeof(UniformBufferObject);
    return buffer_create(&ctx->uniform_buffer, &ctx->_device, bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
			 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
}

bool _context_create_descriptor_pool(Context *ctx) {
    // We'll be creating a two descriptors.
    VkDescriptorPoolSize poolSizes[2] = {};
    poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSizes[0].descriptorCount = 1;
    poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    poolSizes[1].descriptorCount = 1;

    VkDescriptorPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount = ARRAY_LENGTH(poolSizes);
    poolInfo.pPoolSizes = poolSizes;
    poolInfo.maxSets = 1;

    if (vkCreateDescriptorPool(ctx->_device.device, &poolInfo, nullptr, &ctx->descriptorPool) != VK_SUCCESS) {
	fprintf(stderr, "Failed to create descriptor pool!\n");
	return false;
    }
    return true;
}

bool _context_create_descriptor_set(Context *ctx) {
    // Allocate the descriptor set
    VkDescriptorSetLayout layouts[] = {ctx->descriptorSetLayout};
    VkDescriptorSetAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = ctx->descriptorPool;
    allocInfo.descriptorSetCount = 1;
    allocInfo.pSetLayouts = layouts;

    if (vkAllocateDescriptorSets(ctx->_device.device, &allocInfo, &ctx->descriptorSet) != VK_SUCCESS) {
	fprintf(stderr, "Failed to allocate descriptor set!\n");
	return false;
    }

    // Configure the descriptors to put in the descriptor set
    // We've got two descriptors, one for our UBO:
    VkDescriptorBufferInfo bufferInfo = {};
    bufferInfo.buffer = ctx->uniform_buffer.vk_buffer;
    bufferInfo.offset = 0;
    bufferInfo.range = sizeof(UniformBufferObject);

    // And one for the image and sampler resources:
    VkDescriptorImageInfo imageInfo = {};
    imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    imageInfo.imageView = ctx->textureImageView;
    imageInfo.sampler = ctx->textureSampler;

    // Define how our descriptor configuration will get updates
    VkWriteDescriptorSet descriptorWrites[2] = {};

    // UBO descriptor config
    descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[0].dstSet = ctx->descriptorSet;
    descriptorWrites[0].dstBinding = 0; // binding index
    descriptorWrites[0].dstArrayElement = 0; // We're not using an array
    descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorWrites[0].descriptorCount = 1;
    descriptorWrites[0].pBufferInfo = &bufferInfo;
    descriptorWrites[0].pImageInfo = nullptr; // Optional
    descriptorWrites[0].pTexelBufferView = nullptr; // Optional

    // Image descriptor config
    descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[1].dstSet = ctx->descriptorSet;
    descriptorWrites[1].dstBinding = 1; // binding index
    descriptorWrites[1].dstArrayElement = 0; // We're not using an array
    descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptorWrites[1].descriptorCount = 1;
    descriptorWrites[1].pBufferInfo = nullptr;
    descriptorWrites[1].pImageInfo = &imageInfo;
    descriptorWrites[1].pTexelBufferView = nullptr;

    vkUpdateDescriptorSets(ctx->_device.device, ARRAY_LENGTH(descriptorWrites),
			   descriptorWrites, 0, nullptr);
    return true;
}

bool _context_create_command_buffers(Context *ctx) {
    assert(ctx->num_swap_chain_framebuffers < MAX_ARRAY);
    /* TODO
       command_buffers.resize(num_swap_chain_framebuffers);
    */
    ctx->num_command_buffers = ctx->num_swap_chain_framebuffers;

    // Allocate a primary command buffer.  This cannot be called from other
    // command buffers, but can be submitted directly to a queue for execution.
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = ctx->commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = ctx->num_command_buffers;

    if (vkAllocateCommandBuffers(ctx->_device.device, &allocInfo, ctx->command_buffers) != VK_SUCCESS) {
	fprintf(stderr, "Failed to allocate command buffers!\n");
	return false;
    }

    for (size_t i=0; i<ctx->num_command_buffers; i++) {
	// Define how we're going to use this command buffer.  We can tell Vulkan
	// if we plan to rerecord it as soon as it's executed, or that it's a secondary
	// command buffer for use within a single render pass, or that it may be
	// resubmitted while it's already pending execution.  We'll be using the last
	// flag only in this demo.
	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
	beginInfo.pInheritanceInfo = nullptr;

	vkBeginCommandBuffer(ctx->command_buffers[i], &beginInfo);

	// Begin drawing by starting a render pass
	VkRenderPassBeginInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderPass = ctx->renderPass;
	renderPassInfo.framebuffer = ctx->swap_chain_framebuffers[i];

	// Size of the render area
	renderPassInfo.renderArea.offset = {0, 0};
	renderPassInfo.renderArea.extent = ctx->swapChainExtent;

	// Define clear values - black with 100% opacity
	VkClearValue clearColor = {0.0f, 0.0f, 0.0f, 1.0f};
	renderPassInfo.clearValueCount = 1;
	renderPassInfo.pClearValues = &clearColor;

	vkCmdBeginRenderPass(ctx->command_buffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
	vkCmdBindPipeline(ctx->command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, ctx->graphicsPipeline);

	// Bind the vertex buffer during the rendering operations
	VkBuffer vertexBuffers[] = {ctx->vertex_buffer.vk_buffer};
	VkDeviceSize offsets[] = {0};
	vkCmdBindVertexBuffers(ctx->command_buffers[i], 0, 1, vertexBuffers, offsets);

	vkCmdBindIndexBuffer(ctx->command_buffers[i], ctx->index_buffer.vk_buffer, 0, VK_INDEX_TYPE_UINT16);

	// Bind the descriptor set to the graphics pipeline
	vkCmdBindDescriptorSets(ctx->command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS,
				ctx->pipelineLayout, 0, 1, &ctx->descriptorSet, 0, nullptr);

	// Draw triangle with regular vertex buffer
	//vkCmdDraw(command_buffers[i], ARRAY_LENGTH(vertices), 1, 0, 0);

	// Draw indexed buffer
	vkCmdDrawIndexed(ctx->command_buffers[i], num_indices, 1, 0, 0, 0);

	// To draw two triangles, put both of them in the same
	// vertex buffer.  Use the index in vkCmdDraw to specify the
	// one to be drawn:
	//  vkCmdBindVertexBuffers(commandBuffs[i], 0, 1, &vertexBuffer, offsets);
	//  vkCmdDraw(commandBuffs[i],sizeof(vertices) * 2, 1, 0, 0);
	/*
	  VkBuffer vertexBuffs[] = {positionBuffer, colorBuffer};
	  vkCmdBindVertexBuffers(commandBuffs[i], 0, 2, vertexBuffs, offsets);
	  vkCmdDraw(commandBuffs[i],sizeof(vertices), 1, 0, 0);
	*/

	// End the render pass
	vkCmdEndRenderPass(ctx->command_buffers[i]);
	if (vkEndCommandBuffer(ctx->command_buffers[i]) != VK_SUCCESS) {
	    fprintf(stderr, "Failed to record command buffer!\n");
	    return false;
	}
    }

    /*
      you should allocate multiple resources like buffers from a
      single memory allocation, but in fact you should go a step
      further. Driver developers recommend that you also store
      multiple buffers, like the vertex and index buffer, into a
      single VkBuffer and use offsets in commands like
      vkCmdBindVertexBuffers. The advantage is that your data is
      more cache friendly in that case, because it's closer
      together. It is even possible to reuse the same chunk of
      memory for multiple resources if they are not used during the
      same render operations, provided that their data is refreshed,
      of course. This is known as aliasing and some Vulkan functions
      have explicit flags to specify that you want to do this.
    */
    return true;
}

bool _context_create_semaphores(Context *ctx) {
    VkSemaphoreCreateInfo semaphoreInfo = {};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    if (vkCreateSemaphore(ctx->_device.device, &semaphoreInfo, nullptr, &ctx->imageAvailableSemaphore) != VK_SUCCESS ||
	vkCreateSemaphore(ctx->_device.device, &semaphoreInfo, nullptr, &ctx->renderFinishedSemaphore) != VK_SUCCESS) {
	fprintf(stderr, "Failed to create semaphores!\n");
	return false;
    }
    return true;
}

// Makes sure geometry rotates 90 degrees per second regardless of framerate
void context_update_uniform_buffer(Context *ctx) {
    static clock_t start_time = clock();
    clock_t current_time = clock();
    float duration = (double)(current_time - start_time) / CLOCKS_PER_SEC;

    // Define the model, view, and projection transformations
    UniformBufferObject ubo = {};

    // The model rotation will be a simple rotation around the Z-axis
    // This implements the rotation rate of 90 degrees per second
    float angle = duration * M_PI/2.0f;
    float c = cosf(angle);
    float s = sinf(angle);
    ubo.model = cglmMat4(1.0f);
    ubo.model.a0 = c;
    ubo.model.a1 = s;
    ubo.model.b0 = -s;
    ubo.model.b1 = c;

    /* Zooming in and out
       ubo.model.a0 = c;
       ubo.model.a1 = c;
       ubo.model.b0 = -c;
       ubo.model.b1 = c;
    */

    // Look at the geometry from above at a 45 degree angle.
    CGLMvec3 eye_position = { 2.0f, 2.0f, 2.0f };
    CGLMvec3 center_position = { 0.0f, 0.0f, 0.0f };
    CGLMvec3 up_axis = { 0.0f, 0.0f, 1.0f };

    ubo.view = cglmLookAt(eye_position, center_position, up_axis);

    // Apply a perspective projection with a 45 degree vertical field-of-view
    // Use the swap chain for calculating aspect ratio, in case of window resizes
    ubo.proj = cglmPerspective(M_PI/4.0f,
			       ctx->swapChainExtent.width / (float) ctx->swapChainExtent.height,  // Aspect ratio
			       0.1f, 10.0f);   //near and far view planes

    // Invert the Y coordinate so images render right way up
    ubo.proj.b1 *= -1;

    // Copy the data to the buffer directly, now that the transforms are all defined
    void *data;
    vkMapMemory(ctx->_device.device, ctx->uniform_buffer.vk_memory, 0, sizeof(ubo), 0, &data);
    memcpy(data, &ubo, sizeof(ubo));
    vkUnmapMemory(ctx->_device.device, ctx->uniform_buffer.vk_memory);

    // Note: 'push constants' might be better than UBO's for passing
    // frequently changing values to the shader.
}

VkResult context_swap_chain_acquire_next_image(Context *ctx, uint32_t *index) {
    return vkAcquireNextImageKHR(ctx->_device.device, ctx->swapChain, UINT64_MAX,
				 ctx->imageAvailableSemaphore, VK_NULL_HANDLE, index);
}

bool context_submit_draw_command(Context *ctx, uint32_t index) {
    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    // Which semaphores to wait on before execution
    VkSemaphore waitSemaphores[] =  {ctx->imageAvailableSemaphore};
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = waitSemaphores;

    // Command buffers to actually submit
    submitInfo.pWaitDstStageMask = waitStages;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &ctx->command_buffers[index];

    // Which semaphores to signal once finished
    VkSemaphore signalSemaphores[] = {ctx->renderFinishedSemaphore};
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = signalSemaphores;

    // Execute the command buffer for the image
    if (vkQueueSubmit(ctx->_device.graphics_queue, 1, &submitInfo, VK_NULL_HANDLE) != VK_SUCCESS) {
	fprintf(stderr, "Failed to submit draw command buffer!\n");
	return false;
    }
    return true;
}

VkResult context_present(Context *ctx, uint32_t index) {
    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

    // Which semaphores to wait on before presentation
    VkSemaphore signalSemaphores[] = {ctx->renderFinishedSemaphore};
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = signalSemaphores;

    // The swap chain to present image to, and index of the image
    VkSwapchainKHR swapChains[] = {ctx->swapChain};
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;
    presentInfo.pImageIndices = &index;

    // Values to check for every swapchain if presentation was successful.
    // This is not necessary when using a single swapchain, so is optional for us.
    presentInfo.pResults = nullptr;

    // Return the image to the swap chain for presentation
    return vkQueuePresentKHR(ctx->_device.present_queue, &presentInfo);
}

void context_queue_wait_idle(Context *ctx) {
    vkQueueWaitIdle(ctx->_device.present_queue);
}

bool context_init_vulkan(Context *ctx, int32_t w, int32_t h) {
    if (! device_pick_physical_device(&ctx->_device))  return false;
    if (! device_create_logical_device(&ctx->_device)) return false;

    if (!_context_create_swap_chain(ctx, w, h))        return false;
    if (!_context_create_image_views(ctx))             return false;
    if (!_context_create_render_pass(ctx))             return false;
    if (!_context_create_descriptor_set_layout(ctx))   return false;
    if (!context_create_graphics_pipeline(ctx))        return false;
    if (!_context_create_framebuffers(ctx))            return false;
    if (!_context_create_command_pool(ctx))            return false;
    if (!_context_create_texture_image(ctx))           return false;
    if (!_context_create_texture_image_view(ctx))      return false;
    if (!_context_create_texture_sampler(ctx))         return false;
    if (!_context_create_vertex_buffer(ctx))           return false;
    if (!_context_create_index_buffer(ctx))            return false;
    if (!_context_create_uniform_buffer(ctx))          return false;
    if (!_context_create_descriptor_pool(ctx))         return false;
    if (!_context_create_descriptor_set(ctx))          return false;
    if (!_context_create_command_buffers(ctx))         return false;
    if (!_context_create_semaphores(ctx))              return false;

    return true;
}

bool _context_recreate_swap_chain(Context *ctx, int32_t width, int32_t height) {
    vkDeviceWaitIdle(ctx->_device.device);

    _context_cleanup_swap_chain(ctx);

    if (!_context_create_swap_chain(ctx, width, height)) return false;
    if (!_context_create_image_views(ctx))               return false;
    if (!_context_create_render_pass(ctx))               return false;
    if (!context_create_graphics_pipeline(ctx))         return false;
    if (!_context_create_framebuffers(ctx))              return false;
    if (!_context_create_command_buffers(ctx))           return false;

    return true;
}

void context_cleanup(Context *ctx) {
    _context_cleanup_swap_chain(ctx);

    vkDestroySampler(ctx->_device.device, ctx->textureSampler, nullptr);
    vkDestroyImageView(ctx->_device.device, ctx->textureImageView, nullptr);

    image_destroy(&ctx->texture_image);

    // descriptor sets are automatically freed when descriptor pool is destroyed
    vkDestroyDescriptorPool(ctx->_device.device, ctx->descriptorPool, nullptr);
    vkDestroyDescriptorSetLayout(ctx->_device.device, ctx->descriptorSetLayout, nullptr);

    buffer_destroy(&ctx->uniform_buffer);
    buffer_destroy(&ctx->index_buffer);
    buffer_destroy(&ctx->vertex_buffer);

    vkDestroySemaphore(ctx->_device.device, ctx->renderFinishedSemaphore, nullptr);
    vkDestroySemaphore(ctx->_device.device, ctx->imageAvailableSemaphore, nullptr);

    vkDestroyCommandPool(ctx->_device.device, ctx->commandPool, nullptr);

    device_cleanup(&ctx->_device);
}


struct Application {
//private:
    Context ctx;
    GLFWwindow *glfw_window;
};

static void _on_window_resized(GLFWwindow *window, int width, int height) {
    if (width == 0 || height == 0) return;

    Application *app = (Application*)(glfwGetWindowUserPointer(window));

    //int32_t w, h;
    //glfwGetWindowSize(glfw_window, &w, &h);
    _context_recreate_swap_chain(&app->ctx, width, height);
}

void _application_init_window(Application *app) {
    glfwInit();

    // Don't create an OpenGL context, we're going to use Vulkan instead
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    app->glfw_window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan", nullptr, nullptr);

    glfwSetWindowUserPointer(app->glfw_window, app);
    glfwSetWindowSizeCallback(app->glfw_window, _on_window_resized);
}

bool _application_create_surface(Application *app) {
    if (glfwCreateWindowSurface(app->ctx._device.instance,
				app->glfw_window, nullptr,
				&app->ctx._device.surface) != VK_SUCCESS) {
	fprintf(stderr, "Failed to create window surface!\n");
	return false;
    }
    return true;
}

bool _application_draw_frame(Application *app) {
    uint32_t image_index;

    // Acquire an image from the swap chain, to imageIndex
    VkResult result = context_swap_chain_acquire_next_image(&app->ctx, &image_index);
    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
	// Swap chain's become incompatible and can't be used for rendering.
	// Rebuild it and try again next draw_frame call.
	int w, h;
	glfwGetWindowSize(app->glfw_window, &w, &h);
	if (w == 0 || h == 0) return true;

	if (!_context_recreate_swap_chain(&app->ctx, w, h)) {
	    fprintf(stderr, "Failed to recreate swap chain\n");
	    return false;
	}
	return true;
    } else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
	fprintf(stderr, "Failed to acquire swap chain image!\n");
	return false;
    }

    if (!context_submit_draw_command(&app->ctx, image_index)) {
	fprintf(stderr, "Failed to submit draw command!\n");
	return false;
    }

    result = context_present(&app->ctx, image_index);
    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR) {
	int w, h;
	glfwGetWindowSize(app->glfw_window, &w, &h);
	if (w == 0 || h == 0) return true;

	if (!_context_recreate_swap_chain(&app->ctx, w, h)) {
	    fprintf(stderr, "Failed to recreate swap chain\n");
	    return false;
	}
    } else if (result != VK_SUCCESS) {
	fprintf(stderr, "Failed to present swap chain image!\n");
	return false;
    }

    context_queue_wait_idle(&app->ctx);

    // Note: "One thing i want to point out is that using queue wait
    // idle on each frame is not the best thing to do. In fact the
    // performance could be worst than GL.  Best thing to do is use
    // semaphores for each swapchain image rather than using single
    // semaphore. "

    // Alternate drawing scheme, where state must be updated every frame:
    /*
      updateAppState();
      vkQueueWaitIdle(_device.present_queue);
      vkAcquireNextImageKHR(...)
      submitDrawCommands();
      vkQueuePresentKHR(_device.present_queue, &presentInfo);
    */
    // This would permit updating game state, AI, etc. while the
    // previous frame is being rendered, thus keeping both GPU and
    // CPU busy at all times.
    return true;
}

bool _application_main_loop(Application *app) {
    while (!glfwWindowShouldClose(app->glfw_window)) {
	glfwPollEvents();

	context_update_uniform_buffer(&app->ctx);
	if (!_application_draw_frame(app)) {
	    fprintf(stderr, "Error drawing frame\n");
	    return false;
	}
    }

    vkDeviceWaitIdle(app->ctx._device.device);
    return true;
}

void _application_cleanup(Application *app) {
    context_cleanup(&app->ctx);

    glfwDestroyWindow(app->glfw_window);

    glfwTerminate();
}

bool application_run(Application *app) {
    int w, h;
    _application_init_window(app);

    if (!context_init_device_instance(&app->ctx))
	return false;

    if (!_application_create_surface(app))
	return false;

    glfwGetWindowSize(app->glfw_window, &w, &h);
    if (!context_init_vulkan(&app->ctx, w, h))
	return false;

    if (!_application_main_loop(app))
	return false;

    _application_cleanup(app);
    return true;
}

int main() {
    Application app;

    if (! application_run(&app)) {
	    fprintf(stderr, "Error occurred during execution\n");
	    return EXIT_FAILURE;
	}

    return EXIT_SUCCESS;
}
