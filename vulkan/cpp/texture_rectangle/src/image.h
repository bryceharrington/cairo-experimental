#ifndef IMAGE_H
#define IMAGE_H

#include <vulkan/vulkan.h>

struct Device;

struct Image {
	VkDevice       vk_device;
	VkImage        vk_image;
	VkDeviceMemory vk_memory;
};

bool image_create(Image *image, Device *device, uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling,
		  VkImageUsageFlags usage, VkMemoryPropertyFlags properties);

bool image_destroy(Image *image);

#endif /* IMAGE_H */
