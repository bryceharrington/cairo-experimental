#include "device.h"

const char *validation_layers[num_validation_layers] = {
    "VK_LAYER_LUNARG_standard_validation"
};

const char *device_extensions[num_device_extensions] = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

VkResult _device_CreateDebugReportCallbackEXT(VkInstance instance,
                                              const VkDebugReportCallbackCreateInfoEXT *pCreateInfo,
                                              const VkAllocationCallbacks *pAllocator,
                                              VkDebugReportCallbackEXT *pCallback) {
    PFN_vkCreateDebugReportCallbackEXT func;
    func = (PFN_vkCreateDebugReportCallbackEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
    if (func != nullptr) {
        return func(instance, pCreateInfo, pAllocator, pCallback);
    } else {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

void _device_DestroyDebugReportCallbackEXT(VkInstance instance,
                                           VkDebugReportCallbackEXT callback,
                                           const VkAllocationCallbacks *pAllocator) {
    PFN_vkDestroyDebugReportCallbackEXT func;
    func = (PFN_vkDestroyDebugReportCallbackEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
    if (func != nullptr) {
        func(instance, callback, pAllocator);
    }
}

bool _device_check_validation_layer_support() {
    uint32_t num_available_layers;
    vkEnumerateInstanceLayerProperties(&num_available_layers, nullptr);

    assert(num_available_layers < MAX_ARRAY);
    VkLayerProperties available_layers[MAX_ARRAY];
    vkEnumerateInstanceLayerProperties(&num_available_layers, available_layers);

    // Check if all the validation layers we want exist in availableLayers
    for (uint32_t i=0; i<num_validation_layers; i++) {
        const char *layer_name = validation_layers[i];
        bool layer_found = false;

        for (uint32_t j=0; j<num_available_layers; j++) {
            const VkLayerProperties layer_properties = available_layers[j];
            if (strcmp(layer_name, layer_properties.layerName) == 0) {
                layer_found = true;
                break;
            }
        }

        if (!layer_found) {
            return false;
        }
    }

    return true;
}

SwapChainSupportDetails _device_query_swap_chain_support(Device *device, VkPhysicalDevice physical_device) {
    SwapChainSupportDetails details;

    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, device->surface, &details.capabilities);

    // Query the supported surface formats
    vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, device->surface, &details.num_formats, nullptr);
    // Make sure the vector is large enough to hold all the available formats
    assert(details.num_formats < MAX_ARRAY);
    vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, device->surface, &details.num_formats, details.formats);
    /* TODO
       if (formatCount != 0) {
       details.formats.resize(formatCount);
       vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, device->surface, &formatCount, details.formats.data());
       }
    */

    // Query the supported presentation modes
    vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, device->surface, &details.num_present_modes, nullptr);
    assert(details.num_present_modes < MAX_ARRAY);
    vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, device->surface, &details.num_present_modes, details.present_modes);
    /* TODO
       if (presentModeCount != 0) {
       details.presentModes.resize(presentModeCount);
       vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, device->surface, &presentModeCount, details.presentModes.data());
       }
    */

    return details;
}

bool _device_check_device_extension_support(Device *device, VkPhysicalDevice physical_device) {
    // Enumerate the extensions and check if all the required extensions
    // are among them
    uint32_t num_available_extensions;
    vkEnumerateDeviceExtensionProperties(physical_device, nullptr, &num_available_extensions, nullptr);

    assert(num_available_extensions < MAX_ARRAY);
    VkExtensionProperties available_extensions[MAX_ARRAY];
    vkEnumerateDeviceExtensionProperties(
        physical_device, nullptr, &num_available_extensions, available_extensions);

    /* Make sure each of the device extensions we require are present */
    for (uint32_t i=0; i<num_device_extensions; i++) {
        bool found = false;
        for (uint32_t j=0; j<num_available_extensions; j++) {
            if (!strcmp(available_extensions[j].extensionName, device_extensions[i])) {
                found = true;
                break;
            }
        }
        if (! found) {
            /* Required extension was missing; exit with failure */
            return false;
        }
    }

    return true;
}

bool _device_is_device_suitable(Device *device, VkPhysicalDevice physical_device) {
    QueueFamilyIndices indices = device_find_queue_families(device, physical_device);

    bool extensionsSupported = _device_check_device_extension_support(device, physical_device);

    bool swapChainAdequate = false;
    if (extensionsSupported) {
        SwapChainSupportDetails swapChainSupport = _device_query_swap_chain_support(device, physical_device);
        swapChainAdequate = (swapChainSupport.num_formats != 0)
            && (swapChainSupport.num_present_modes != 0);
    }

    VkPhysicalDeviceFeatures supportedFeatures;
    vkGetPhysicalDeviceFeatures(physical_device, &supportedFeatures);

    return indices.isComplete() && extensionsSupported && swapChainAdequate && supportedFeatures.samplerAnisotropy;
}


bool device_setup_debug_callback(Device *device, PFN_vkDebugReportCallbackEXT callback) {
    if (!enableValidationLayers)
        return true;

    VkDebugReportCallbackCreateInfoEXT createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
    createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
    createInfo.pfnCallback = callback;

    // Lookup & load the report callback creation extension function
    if (_device_CreateDebugReportCallbackEXT(device->instance, &createInfo, nullptr, &device->_debug_callback) != VK_SUCCESS) {
        fprintf(stderr, "Failed to set up debug callback!");
        return false;
    }
    return true;
}

bool device_create_instance(Device *device, const char *extensions[], int32_t num_extensions) {
    device->physical_device = VK_NULL_HANDLE;

    // Enable validation layers
    if (enableValidationLayers && !_device_check_validation_layer_support()) {
        fprintf(stderr, "Validation layers requested, but not available!");
        return false;
    }

    VkApplicationInfo appInfo = {};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "Texture Rectangle Demo";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "No Engine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;

    createInfo.enabledExtensionCount = num_extensions;
    createInfo.ppEnabledExtensionNames = extensions;

    // Setup validation layers
    if (enableValidationLayers) {
        createInfo.enabledLayerCount = num_validation_layers;
        createInfo.ppEnabledLayerNames = validation_layers;
    } else {
        createInfo.enabledLayerCount = 0;
    }

    /* FIXME: create a function that checks if all of the extensions
     * returned by glfwGetRequiredInstanceExtensions are included in
     * the supported extensions list.
     * c.f.: https://vulkan-tutorial.com/Drawing_a_triangle/Setup/Instance#comment-2958000785
     */

    if (vkCreateInstance(&createInfo, nullptr, &device->instance) != VK_SUCCESS) {
        fprintf(stderr, "Failed to create instance!");
        return false;
    }
    return true;
}

bool device_pick_physical_device(Device *device) {
    uint32_t num_devices = 0;
    vkEnumeratePhysicalDevices(device->instance, &num_devices, nullptr);

    if (num_devices == 0) {
        fprintf(stderr, "Failed to find GPUs with Vulkan support!");
        return false;
    }

    assert(num_devices < MAX_ARRAY);
    VkPhysicalDevice vk_physical_devices[MAX_ARRAY];
    vkEnumeratePhysicalDevices(device->instance, &num_devices, vk_physical_devices);

    for (const VkPhysicalDevice &vk_physical_device : vk_physical_devices) {
        if (_device_is_device_suitable(device, vk_physical_device)) {
            device->physical_device = vk_physical_device;
            break;
        }
    }

    if (device->physical_device == VK_NULL_HANDLE) {
        fprintf(stderr, "Failed to find a suitable GPU!");
        return false;
    }
    return true;
}

bool device_create_logical_device(Device *device) {
    QueueFamilyIndices indices = device_find_queue_families(device, device->physical_device);

    uint32_t num_queue_create_infos = 0;
    VkDeviceQueueCreateInfo queue_create_infos[MAX_ARRAY];
    const int num_unique_queue_families = 2;
    int unique_queue_families[num_unique_queue_families] = {
        indices.graphicsFamily,
        indices.presentFamily
    };

    float queue_priority = 1.0f;
    for (int queue_family = 0; queue_family < num_unique_queue_families; queue_family++) {
        VkDeviceQueueCreateInfo queueCreateInfo = {};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = queue_family;
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &queue_priority;
        queue_create_infos[num_queue_create_infos] = queueCreateInfo;
        num_queue_create_infos++;
    }

    // Define the set of device features we'll be using, like geometry shaders
    VkPhysicalDeviceFeatures deviceFeatures = {};
    deviceFeatures.samplerAnisotropy = VK_TRUE;

    // Set up parameters for logical device creation
    VkDeviceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.queueCreateInfoCount = num_queue_create_infos;
    createInfo.pQueueCreateInfos = queue_create_infos;
    createInfo.pEnabledFeatures = &deviceFeatures;

    createInfo.enabledExtensionCount = num_device_extensions;
    createInfo.ppEnabledExtensionNames = device_extensions;

    // Enable validation layers for the devices
    if (enableValidationLayers) {
        createInfo.enabledLayerCount = num_validation_layers;
        createInfo.ppEnabledLayerNames = validation_layers;
    } else {
        createInfo.enabledLayerCount = 0;
    }

    // Instantiate the logical device
    if (vkCreateDevice(device->physical_device, &createInfo, nullptr, &device->device) != VK_SUCCESS) {
        fprintf(stderr, "Failed to create logical device!");
        return false;
    }

    // Retrieve queue handles
    vkGetDeviceQueue(device->device, indices.graphicsFamily, 0, &device->graphics_queue);
    vkGetDeviceQueue(device->device, indices.presentFamily, 0, &device->present_queue);
    return true;
}

/* Queue family lookup function */
QueueFamilyIndices device_find_queue_families(Device *device, VkPhysicalDevice physical_device) {
    QueueFamilyIndices queue_family_indices;

    // Get number of queue families
    uint32_t num_queue_families = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &num_queue_families, nullptr);

    // Retrieve the queue families
    assert(num_queue_families < MAX_ARRAY);
    VkQueueFamilyProperties queue_families[MAX_ARRAY];
    vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &num_queue_families, queue_families);

    // Find at least one graphical queue family
    for (int i=0; i<num_queue_families; i++) {
        if (queue_families[i].queueCount > 0 && queue_families[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
            queue_family_indices.graphicsFamily = i;
        }

        VkBool32 presentSupport = false;
        vkGetPhysicalDeviceSurfaceSupportKHR(physical_device, i, device->surface, &presentSupport);

        if (queue_families[i].queueCount > 0 && presentSupport) {
            queue_family_indices.presentFamily = i;
        }

        if (queue_family_indices.isComplete()) {
            break;
        }
    }

    return queue_family_indices;
}

// Examine graphics card's available type of memory.  Get index of first matching our requirements.
uint32_t device_find_memory_type(Device *device, uint32_t typeFilter, VkMemoryPropertyFlags properties) {
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(device->physical_device, &memProperties);

    for (uint32_t i=0; i<memProperties.memoryTypeCount; i++) {
        if ((typeFilter & (1 << i)) &&
            (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
            return i;
        }
    }

    fprintf(stderr, "Failed to find suitable memory type!\n");
    assert(1); // TODO: Error return
}

SwapChainSupportDetails device_get_swap_chain_support(Device *device) {
    return _device_query_swap_chain_support(device, device->physical_device);
}

QueueFamilyIndices device_get_queue_families(Device *device) {
    return device_find_queue_families(device, device->physical_device);
}

void device_cleanup(Device *device) {
    vkDestroyDevice(device->device, nullptr);
    _device_DestroyDebugReportCallbackEXT(device->instance, device->_debug_callback, nullptr);
    vkDestroySurfaceKHR(device->instance, device->surface, nullptr);
    vkDestroyInstance(device->instance, nullptr);
}

