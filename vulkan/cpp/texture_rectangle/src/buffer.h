#ifndef BUFFER_H
#define BUFFER_H

#include <vulkan/vulkan.h>

struct Device;

struct Buffer {
    VkDevice       vk_device;
    VkBuffer       vk_buffer;
    VkDeviceMemory vk_memory;
};

bool buffer_create(Buffer *buffer, Device *device, VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties);

bool buffer_destroy(Buffer *buffer);


#endif /* BUFFER_H */
